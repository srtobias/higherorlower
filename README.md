# README #

This is a Higher or Lower game. Create a game and use the generated token to make a play.

You have to guess if the next card in the deck is higher or lower than the current card.

### Description ###

This project is made using .NET core 5.0 and Entity Framework.

### How do I get set up? ###
To run the project use the command below:

`docker-compose up higherorlower`

Access to `http://container_ip:8080/swagger` to check API documentation

### Contribution guidelines ###

* Database using SQLite
* Project provided in docker container
* Unit testing
* Tracing
* Dependency Injection
* REST service written in C#
* Authentication token to validate allowed requests

### Author ###

* Sergio Pinto - sergiopinto178@gmail.com