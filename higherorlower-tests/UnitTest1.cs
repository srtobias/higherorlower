using Microsoft.VisualStudio.TestTools.UnitTesting;
using HigherOrLowerApi.Models;
using HigherOrLowerApi.Data;
using HigherOrLowerApi.Managers;
using HigherOrLowerApi;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Logging;

namespace higherorlower_tests
{
    [TestClass]
    public class UnitTest1
    {
        public UnitTest1(){            
            AppLogging.LoggerFactory = LoggerFactory.Create(builder => builder.AddConsole());
        }
        [TestMethod]
        public void TestShuffle()
        {
            IGameData _context = new GameInMemoryData();
            GameManager _manager = new GameManager();
            Game g1 = new Game();
            g1.InitializeGame(_manager.GetUnshuffledDeck());
            CollectionAssert.AreNotEqual(g1.CurrentGame.Cards, _manager.GetUnshuffledDeck().Cards);
        }

        [TestMethod]
        public void TestVictory1(){
            GameManager gm = new GameManager();
            Card c1 = new Card(2,Enums.Suit.Clubs);
            Card c2 = new Card(14,Enums.Suit.Clubs);

            Assert.IsTrue(gm.GetVictory(c1,c2,true));            
        }
        [TestMethod]
        public void TestVictory2(){
            GameManager gm = new GameManager();
            Card c1 = new Card(14,Enums.Suit.Clubs);
            Card c2 = new Card(13,Enums.Suit.Clubs);

            Assert.IsFalse(gm.GetVictory(c1,c2,true));            
        }
        
        [TestMethod]
        public void TestVictory3(){
            GameManager gm = new GameManager();
            Card c1 = new Card(14,Enums.Suit.Clubs);
            Card c2 = new Card(14,Enums.Suit.Clubs);

            Assert.IsTrue(gm.GetVictory(c1,c2,true));            
        }
        
        [TestMethod]
        public void TestVictory4(){
            GameManager gm = new GameManager();
            Card c1 = new Card(14,Enums.Suit.Clubs);
            Card c2 = new Card(14,Enums.Suit.Clubs);

            Assert.IsTrue(gm.GetVictory(c1,c2,false));            
        }
        
        [TestMethod]
        public void TestVictory5(){
            GameManager gm = new GameManager();
            Card c1 = new Card(13,Enums.Suit.Clubs);
            Card c2 = new Card(14,Enums.Suit.Clubs);

            Assert.IsFalse(gm.GetVictory(c1,c2,false));            
        }
        
        [TestMethod]
        public void TestVictory6(){
            GameManager gm = new GameManager();
            Card c1 = new Card(13,Enums.Suit.Clubs);
            Card c2 = new Card(2,Enums.Suit.Clubs);

            Assert.IsTrue(gm.GetVictory(c1,c2,false));            
        }

        [TestMethod]
        public void TestBase64Token(){            
            GameManager _manager = new GameManager();
            Game g1 = new Game();
            Assert.IsTrue(Regex.IsMatch(g1.AuthToken, @"^[a-zA-Z0-9\+/]*={0,2}$"));
        }

        [TestMethod]
        public void TestGameCreation(){            
            IGameData _context = new GameInMemoryData();
            GameManager _manager = new GameManager();
            Game g1 = new Game();
            g1.InitializeGame(_manager.GetUnshuffledDeck());
            _context.Add(g1);

            Assert.AreEqual(g1,_context.GetGameById(g1.Id));
        }

        [TestMethod]
        public void TestPeek(){
            GameManager _manager = new GameManager();
            Game g1 = new Game();
            g1.InitializeGame(_manager.GetUnshuffledDeck());
            Card lastCard = g1.CardInPlay;
            int cardsToPlay = g1.CurrentGame.Cards.Count;
            g1.PeekCard();
            Assert.AreNotEqual(lastCard,g1.CardInPlay);
            Assert.AreNotEqual(cardsToPlay, g1.CurrentGame.Cards.Count);
        }
    }
}
