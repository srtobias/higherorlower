using System;

namespace HigherOrLowerApi.Models{
    public class GameDisplay{
        public int Id { get; set; }
        public String CardInPlay{ get; set; }

        public GameDisplay(int id, Card cardInPlay)
        {
            this.Id = id;
            this.CardInPlay = cardInPlay.ToString();
        }
    }
}