using System;

namespace HigherOrLowerApi.Models{
    public class PlayResult{
        public int Id { get; set; }
        public String Message{ get; set; }
        public bool Victory{ get; set; }

        public PlayResult(int id, String Message, bool Victory)
        {
            this.Id = id;
            this.Message = Message;
            this.Victory = Victory;
        }
    }
}