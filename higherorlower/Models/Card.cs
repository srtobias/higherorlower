using System;

namespace HigherOrLowerApi.Models{
    public class Card{
        public Int32 Id { get; set; }
        public Int32 Value { get; set; }
        public Enums.Suit Suit{ get; set; }
        public Card(Int32 value, Enums.Suit suit)
        {
            this.Value = value;
            this.Suit = suit;
        }

        public override String ToString(){
            String result = "";
            switch(this.Value){
                case 11:
                    result = "Jack ";
                    break;
                case 12:
                    result = "Queen ";
                    break;
                case 13:
                    result = "King ";
                    break;
                case 14:
                    result = "Ace ";
                    break;
                default:
                    result = this.Value.ToString() + " ";
                    break;
            }
            result += "of " + this.Suit.ToString();
            return result;
        }
    }
}