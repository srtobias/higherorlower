using System.Linq;
using System;
using Microsoft.Extensions.Logging;
namespace HigherOrLowerApi.Models{
    [Serializable]
    public class Game{
        public int Id { get; set; }
        public string AuthToken{ get; set; }
        public virtual Deck CurrentGame { get; set; }
        public virtual Card CardInPlay{ get; set; }
        private readonly ILogger _logger;
        
        public Game(){
            this._logger = AppLogging.CreateLogger("Game");
            this.AuthToken = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
        }

        public void InitializeGame(){
            this._logger.LogInformation("> InitializeGame");
            this.CurrentGame = new Deck();
            this.CurrentGame.AddCardsToDeck();
            this._logger.LogDebug(String.Format("Deck size: {0}",this.CurrentGame.Cards.Count()));
            this.CurrentGame.ShuffleCards();
            this.PeekCard();
            this._logger.LogInformation("< InitializeGame");

        }
        public Card PeekCard(){
            this._logger.LogDebug(String.Format("Before peek - CardInPlay: {0}",this.CardInPlay));
            this.CardInPlay = this.CurrentGame.Cards.First();
            this._logger.LogDebug(String.Format("After peek - CardInPlay: {0}",this.CardInPlay));
            this.CurrentGame.Cards.Remove(this.CardInPlay);
            this._logger.LogDebug(String.Format("Remaining cards in deck: {0}",this.CurrentGame.Cards.Count()));
            return this.CardInPlay;
        }
    }
}