using System;
using System.Collections.Generic;

namespace HigherOrLowerApi.Models{

    [Serializable]
    public class Deck{
        public Int32 Id { get; set; }
        public virtual List<Card> Cards { get; set; }
        
        public Deck()
        {
        }

        public void AddCardsToDeck(){
            Cards = new List<Card>();
            foreach(Enums.Suit s in Enum.GetValues(typeof(Enums.Suit))){
                for (int i=2; i<= 14; i++){
                    this.Cards.Add(new Card(i, s));
                }
            }
        }
        public void ShuffleCards(){
            this.Cards.Shuffle();
        }

    }
}