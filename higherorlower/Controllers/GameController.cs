using System.Diagnostics;
using System.Net;
using System;
using Microsoft.AspNetCore.Mvc;
using HigherOrLowerApi.Models;
using HigherOrLowerApi.Data;
using HigherOrLowerApi.Managers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using HigherOrLowerApi; 
namespace HigherOrLowerApi.Controllers
{          
    [Route("/api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    public class GameController : ControllerBase
    {
        private readonly IGameData _context;
        private readonly GameManager _manager;
        private readonly ILogger _logger;

        public GameController(IGameData context, GameManager manager)
        {
            _context = context;
            _manager = manager;
            _logger = AppLogging.CreateLogger("GameController");
        }

        /// <summary>
        /// Create a new game
        /// </summary>
        /// <returns>Created game identificator and authentication token</returns>
        [Route("create")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<Game> CreateGame(){
            _logger.LogInformation("> CreateGame");
            _logger.LogDebug("Creating new game");
            Game _result = new Game();
            _logger.LogDebug(String.Format("Game created with id={0}", _result.Id));
            _result.InitializeGame();
            _logger.LogDebug(String.Format("Adding game (id={0}) to data store", _result.Id));
            _context.Add(_result);
            _logger.LogInformation("< CreateGame");
            return CreatedAtAction("GetGame", new { id = _result.Id }, new {Id = _result.Id, AuthToken = _result.AuthToken});
        }

        /// <summary>
        /// Get information about the game
        /// </summary>
        /// <param name="id">Game identificator</param>
        /// <returns>Game info</returns>
        [HttpGet("{id}")]        
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<GameDisplay> GetGame(int id)
        {            
            _logger.LogInformation(String.Format("> GetGame - id:{0}",id));
            Game game = _context.GetGameById(id);
            if (game == null)
            {
                _logger.LogInformation(String.Format("Game with id {0} not found",id));
                return NotFound();
            }
            _logger.LogInformation("< GetGame");
            return new GameDisplay(game.Id, game.CardInPlay);
        }

        // POST: api/Game/5
        /// <summary>
        /// Commit a play inside a game
        /// </summary>
        /// <param name="id">Game identificator</param>
        /// <param name="authToken">Authentication token to validate authorized play</param>
        /// <param name="higher">Play guess</param>
        /// <returns>A newly PlayResult object</returns>
        [HttpPost("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(String),StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(String),StatusCodes.Status500InternalServerError)]
        public ActionResult<PlayResult> GetGame(int id, [FromHeader] String authToken,[FromBody] bool higher)
        {
            
            _logger.LogInformation(String.Format("> POST GetGame - id:{0}, authToken:{1}, higher:{2}",id,authToken,higher));
            Game game = _context.GetGameById(id);

            if (game == null){                
                _logger.LogInformation(String.Format("Game with id {0} not found",id));
                return NotFound();
            }
            if(game.AuthToken != authToken){                
                _logger.LogInformation(String.Format("Token does not match"));
                return Unauthorized();
            }
            if(game.CurrentGame.Cards.Count == 0){                
                _logger.LogDebug(String.Format("No more cards to play in this game"));
                return StatusCode((int) HttpStatusCode.Forbidden, "No more cards to play in this game");
            }

            try{     
                var _result = _manager.GetPlayResult(game,higher);
                _context.UpdateGame(_result.Item1);
                return _result.Item2;
            }catch(Exception ex){                
                _logger.LogError(String.Format("{0} : {1}",ex.Message,ex.StackTrace));
                return StatusCode((int) HttpStatusCode.InternalServerError, "Something was wrong updating the deck.");
            }
        }
    }
}
