using Microsoft.EntityFrameworkCore;
using HigherOrLowerApi.Models;

namespace HigherOrLowerApi.Data
{
    public class GameDbContext : DbContext
    {
        public GameDbContext(DbContextOptions<GameDbContext> options)
            : base(options)
        {
        }

        public DbSet<Game> GameItems { get; set; }
        public DbSet<Card> CardItems { get; set; }
        public DbSet<Deck> DeckItems { get; set; }

    }
}