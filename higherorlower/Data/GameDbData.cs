using System;
using HigherOrLowerApi.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace HigherOrLowerApi.Data
{
    public class GameDbData : IGameData
    {
        private readonly GameDbContext db;
        
        private readonly ILogger _logger;

        
        public GameDbData(GameDbContext db)
        {
            this.db = db;
            this._logger = AppLogging.CreateLogger("GameDbData");
        }
        public void Add(Game newGame){
            _logger.LogInformation(String.Format("> AddGame - id:{0}",newGame.Id));
            db.GameItems.Add(newGame);
            db.SaveChanges();
            _logger.LogInformation("< AddGame");
        }
        public void UpdateGame(Game game){

            _logger.LogInformation(String.Format("> UpdateGame - id:{0}",game.Id));
            db.GameItems.Attach(game);
            db.SaveChanges();
            _logger.LogInformation("< UpdateGame ");
        }
        public Game GetGameById(int id){
            _logger.LogInformation(String.Format("> GetGameById - id:{0}",id));
            var game = db.GameItems
                .Include(g => g.CurrentGame)
                .Include(g => g.CardInPlay)
                .Include(g => g.CurrentGame.Cards)
                .SingleOrDefault(g => g.Id == id);
            if(game!=null)
                _logger.LogDebug(String.Format("< GetGameById - id:{0}, authToken:{1}, cardInPlay:{2}, deckSize:{3}"
                    ,game.Id, game.AuthToken, game.CardInPlay, game.CurrentGame.Cards.Count()));
            return game;
        }

    }
}