using HigherOrLowerApi.Models;

namespace HigherOrLowerApi.Data{
    public interface IGameData{
        public void Add(Game newGame);
        public Game GetGameById(int id);
        public void UpdateGame(Game game);
    }
}