using System.Linq;
using System.Collections.Generic;
using HigherOrLowerApi.Models;

namespace HigherOrLowerApi.Data
{
    public class GameInMemoryData : IGameData
    {
        public GameInMemoryData()
        {
            gameItems = new List<Game>();
        }
        List<Game> gameItems { get; set; }

        public void Add(Game newGame){
            this.gameItems.Add(newGame);
        }
        public void UpdateGame(Game game){
            // does nothing because the structure is updated automatically
            return;
        }
        public Game GetGameById(int id){
            return gameItems.SingleOrDefault(r => r.Id == id);
        }
    }
}