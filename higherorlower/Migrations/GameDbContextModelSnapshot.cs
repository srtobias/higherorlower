﻿// <auto-generated />
using System;
using HigherOrLowerApi.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace HigherOrLowerApi.Migrations
{
    [DbContext(typeof(GameDbContext))]
    partial class GameDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "5.0.5");

            modelBuilder.Entity("HigherOrLowerApi.Models.Card", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int?>("DeckId")
                        .HasColumnType("INTEGER");

                    b.Property<int>("Suit")
                        .HasColumnType("INTEGER");

                    b.Property<int>("Value")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.HasIndex("DeckId");

                    b.ToTable("CardItems");
                });

            modelBuilder.Entity("HigherOrLowerApi.Models.Deck", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.ToTable("DeckItems");
                });

            modelBuilder.Entity("HigherOrLowerApi.Models.Game", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("AuthToken")
                        .HasColumnType("TEXT");

                    b.Property<int?>("CardInPlayId")
                        .HasColumnType("INTEGER");

                    b.Property<int?>("CurrentGameId")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.HasIndex("CardInPlayId");

                    b.HasIndex("CurrentGameId");

                    b.ToTable("GameItems");
                });

            modelBuilder.Entity("HigherOrLowerApi.Models.Card", b =>
                {
                    b.HasOne("HigherOrLowerApi.Models.Deck", null)
                        .WithMany("Cards")
                        .HasForeignKey("DeckId");
                });

            modelBuilder.Entity("HigherOrLowerApi.Models.Game", b =>
                {
                    b.HasOne("HigherOrLowerApi.Models.Card", "CardInPlay")
                        .WithMany()
                        .HasForeignKey("CardInPlayId");

                    b.HasOne("HigherOrLowerApi.Models.Deck", "CurrentGame")
                        .WithMany()
                        .HasForeignKey("CurrentGameId");

                    b.Navigation("CardInPlay");

                    b.Navigation("CurrentGame");
                });

            modelBuilder.Entity("HigherOrLowerApi.Models.Deck", b =>
                {
                    b.Navigation("Cards");
                });
#pragma warning restore 612, 618
        }
    }
}
