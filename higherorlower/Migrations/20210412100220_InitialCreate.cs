﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HigherOrLowerApi.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DeckItems",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeckItems", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CardItems",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Value = table.Column<int>(type: "INTEGER", nullable: false),
                    Suit = table.Column<int>(type: "INTEGER", nullable: false),
                    DeckId = table.Column<int>(type: "INTEGER", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CardItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CardItems_DeckItems_DeckId",
                        column: x => x.DeckId,
                        principalTable: "DeckItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GameItems",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    AuthToken = table.Column<string>(type: "TEXT", nullable: true),
                    CurrentGameId = table.Column<int>(type: "INTEGER", nullable: true),
                    CardInPlayId = table.Column<int>(type: "INTEGER", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GameItems_CardItems_CardInPlayId",
                        column: x => x.CardInPlayId,
                        principalTable: "CardItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GameItems_DeckItems_CurrentGameId",
                        column: x => x.CurrentGameId,
                        principalTable: "DeckItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CardItems_DeckId",
                table: "CardItems",
                column: "DeckId");

            migrationBuilder.CreateIndex(
                name: "IX_GameItems_CardInPlayId",
                table: "GameItems",
                column: "CardInPlayId");

            migrationBuilder.CreateIndex(
                name: "IX_GameItems_CurrentGameId",
                table: "GameItems",
                column: "CurrentGameId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GameItems");

            migrationBuilder.DropTable(
                name: "CardItems");

            migrationBuilder.DropTable(
                name: "DeckItems");
        }
    }
}
