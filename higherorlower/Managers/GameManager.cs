using System;
using HigherOrLowerApi.Models;
using Microsoft.Extensions.Logging;

namespace HigherOrLowerApi.Managers
{
    public class GameManager
    {

        private readonly ILogger _logger;

        public GameManager()
        {
            this._logger = AppLogging.CreateLogger("GameManager");
        }

        public Tuple<Game,PlayResult> GetPlayResult(Game game, bool higher){
            _logger.LogInformation(String.Format("> GetPlayResult - id:{0}, higher:{1}",game.Id, higher));
            Card lastCard = game.CardInPlay;
            Card newCard = game.PeekCard();
            _logger.LogDebug(String.Format("NewCard:{0} , LastCard:{1}",newCard,lastCard));
            bool victory = GetVictory(lastCard, newCard, higher);           
            String message = String.Format("Last card was: {0}; New card is: {1}; YOU {2}"
                , lastCard.ToString()
                , newCard.ToString()
                , victory?"WON":"LOST"); 

            _logger.LogDebug(String.Format("Victory:{0}, Message:{1}",victory,message));
            _logger.LogInformation("< GetPlayResult");
            return new Tuple<Game,PlayResult>(game, new PlayResult(game.Id, message, victory));
        }

        //public method for UnitTests
        public bool GetVictory(Card lastCard, Card newCard, bool higher){            
            if(higher && newCard.Value >= lastCard.Value){
                return true;
            }else if(!higher && newCard.Value <= lastCard.Value){
                return true;
            }
            return false;
        }
    }
}